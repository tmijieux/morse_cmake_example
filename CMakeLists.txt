cmake_minimum_required(VERSION 2.8)
project(morse_cmake_example C Fortran)

set(MORSE_CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules/morse_cmake/modules/")
list(APPEND CMAKE_MODULE_PATH "${MORSE_CMAKE_MODULE_PATH}")
list(APPEND CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake_modules/")
include(MorseInit)

option(BUILD_SHARED_LIBS "build shared libraries" OFF)

find_package(CHAMELEON COMPONENTS STARPU MPI REQUIRED)

add_library(example_lib example_lib.c)

target_compile_definitions(example_lib PRIVATE ${CHAMELEON_DEFINITIONS} )
target_include_directories(example_lib PRIVATE ${CHAMELEON_INCLUDE_DIRS} )
target_link_libraries(example_lib ${CHAMELEON_LIBRARIES})

add_executable(example_main main.c)
target_link_libraries(example_main example_lib)

install(TARGETS example_lib DESTINATION lib)
install(TARGETS example_main DESTINATION bin)
