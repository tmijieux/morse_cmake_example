# - Try to find chameleon
# Once done, this will define:
#
# CHAMELEON_FOUND - system has Chameleon
# CHAMELEON_LIBRARIES - chameleon libraries to link to
# CHAMELEON_DEFINITIONS - chameleon definitions
# CHAMELEON_INCLUDE_DIRS - chameleon include directories

macro(FABULOUS_FIND_STATIC_LIBRARIES _prefix _pc_prefix)
    unset(${_prefix}_LIBRARIES)
    unset(${_prefix}_LIBRARIES CACHE)
    set(${_prefix}_LIBRARIES "")
    foreach(_library ${${_pc_prefix}_STATIC_LIBRARIES})
        string(CONCAT _library_prefixed "lib" ${_library})
        find_library(_library_path NAMES ${_library} ${_library_prefixed}
            HINTS ${${_pc_prefix}_STATIC_LIBDIR} ${${_pc_prefix}_STATIC_LIBRARY_DIRS} )
        if (EXISTS "${_library_path}")
            list(APPEND ${_prefix}_LIBRARIES ${_library_path})
        else()
            message(FATAL_ERROR "Dependency of ${_prefix} '${_library}' NOT FOUND")
        endif()
        unset(_library_path CACHE)
        unset(_library_path)
    endforeach()
    #list(REMOVE_DUPLICATES ${_prefix}_LIBRARIES)
    #set(${_prefix}_LIBRARIES ${${_prefix}_LIBRARIES} CACHE STRING "list of libraries for ${_prefix}")
    #unset(${_prefix}_LIBRARIES)
endmacro()

# macro(FABULOUS_FIND_STATIC_LIBRARIES _prefix _pc_prefix)
#     unset(${_prefix}_LIBRARIES)
#     unset(${_prefix}_LIBRARIES CACHE)
#     set(${_prefix}_LIBRARIES "")
#
#     foreach(_library ${${_pc_prefix}_STATIC_LIBRARIES})
#         unset(_library_path CACHE)
#         unset(_library_path)
#         string(CONCAT _lib_name_prefixed "lib" ${_library} ".a")
#         find_library(_library_path NAMES ${_lib_name_prefixed}
#             HINTS ${${_pc_prefix}_STATIC_LIBDIR} ${${_pc_prefix}_STATIC_LIBRARY_DIRS} )
#         if (EXISTS "${_library_path}")
#             list(APPEND ${_prefix}_LIBRARIES ${_library_path})
#         else()
#             #string(CONCAT _lib_name ${_library} ".so")
#             string(CONCAT _lib_name_prefixed "lib" ${_library} ".so")
#             unset(_library_path CACHE)
#             unset(_library_path)
#             find_library(_library_path NAMES ${_lib_name} ${_lib_name_prefixed}
#                 HINTS ${${_pc_prefix}_STATIC_LIBDIR} ${${_pc_prefix}_STATIC_LIBRARY_DIRS} )
#             if (EXISTS "${_library_path}")
#                 list(APPEND ${_prefix}_LIBRARIES ${_library_path})
#             endif()
#         endif()
#     endforeach()
#     #list(REMOVE_DUPLICATES ${_prefix}_LIBRARIES)
#     #set(${_prefix}_LIBRARIES ${${_prefix}_LIBRARIES} CACHE STRING "list of libraries for ${_prefix}")
#     #unset(${_prefix}_LIBRARIES)
# endmacro()

macro(FABULOUS_FIND_SHARED_LIBRARIES _prefix _pc_prefix)
    unset(${_prefix}_LIBRARIES)
    unset(${_prefix}_LIBRARIES CACHE)
    set(${_prefix}_LIBRARIES "")
    foreach(_library ${${_pc_prefix}_LIBRARIES})
        string(CONCAT _library_prefixed "lib" ${_library})
        find_library(_library_path NAMES ${_library} ${_library_prefixed}
            HINTS ${${_pc_prefix}_LIBDIR} ${${_pc_prefix}_LIBRARY_DIRS} )
        if (EXISTS "${_library_path}")
            list(APPEND ${_prefix}_LIBRARIES ${_library_path})
        else()
            message(FATAL_ERROR "Dependency of ${_prefix} '${_library}' NOT FOUND")
        endif()
        unset(_library_path CACHE)
        unset(_library_path)
    endforeach()
    #list(REMOVE_DUPLICATES ${_prefix}_LIBRARIES)
    #set(${_prefix}_LIBRARIES ${${_prefix}_LIBRARIES} CACHE STRING "list of libraries for ${_prefix}")
    #unset(${_prefix}_LIBRARIES)
endmacro()

find_package(PkgConfig REQUIRED)
pkg_check_modules(PC_CHAMELEON QUIET chameleon)

#[=====================================================================================[
message("")
message( "PC_CHAMELEON_FOUND         = ${PC_CHAMELEON_FOUND}" )
message( "PC_CHAMELEON_LIBRARIES     = ${PC_CHAMELEON_LIBRARIES}" )
message( "PC_CHAMELEON_LIBRARY_DIRS  = ${PC_CHAMELEON_LIBRARY_DIRS}")
message( "PC_CHAMELEON_LDFLAGS       = ${PC_CHAMELEON_LDFLAGS}")
message( "PC_CHAMELEON_LDFLAGS_OTHER = ${PC_CHAMELEON_LDFLAGS_OTHER}")
message( "PC_CHAMELEON_INCLUDE_DIRS  = ${PC_CHAMELEON_INCLUDE_DIRS}")
message( "PC_CHAMELEON_CFLAGS        = ${PC_CHAMELEON_CFLAGS}")
message( "PC_CHAMELEON_CFLAGS_OTHER  = ${PC_CHAMELEON_CFLAGS_OTHER}")
message("")
message( "PC_CHAMELEON_STATIC_FOUND         = ${PC_CHAMELEON_STATIC_FOUND}" )
message( "PC_CHAMELEON_STATIC_LIBRARIES     = ${PC_CHAMELEON_STATIC_LIBRARIES}" )
message( "PC_CHAMELEON_STATIC_LIBRARY_DIRS  = ${PC_CHAMELEON_STATIC_LIBRARY_DIRS}")
message( "PC_CHAMELEON_STATIC_LDFLAGS       = ${PC_CHAMELEON_STATIC_LDFLAGS}")
message( "PC_CHAMELEON_STATIC_LDFLAGS_OTHER = ${PC_CHAMELEON_STATIC_LDFLAGS_OTHER}")
message( "PC_CHAMELEON_STATIC_INCLUDE_DIRS  = ${PC_CHAMELEON_STATIC_INCLUDE_DIRS}")
message( "PC_CHAMELEON_STATIC_CFLAGS        = ${PC_CHAMELEON_STATIC_CFLAGS}")
message( "PC_CHAMELEON_STATIC_CFLAGS_OTHER  = ${PC_CHAMELEON_STATIC_CFLAGS_OTHER}")
message("")
# ]=====================================================================================]

### detect STATIC
find_path(CHAMELEON_STATIC_INCLUDE_DIR NAMES morse.h
    HINTS ${PC_CHAMELEON_STATIC_INCLUDEDIR} ${PC_CHAMELEON_STATIC_INCLUDE_DIRS} )
find_library(CHAMELEON_STATIC_LIBRARY NAMES libchameleon.a
    HINTS ${PC_CHAMELEON_STATIC_LIBDIR} ${PC_CHAMELEON_STATIC_LIBRARY_DIRS} )
### detect SHARED
find_path(CHAMELEON_SHARED_INCLUDE_DIR NAMES morse.h
    HINTS ${PC_CHAMELEON_INCLUDEDIR} ${PC_CHAMELEON_INCLUDE_DIRS} )
find_library(CHAMELEON_SHARED_LIBRARY NAMES libchameleon.so
    HINTS ${PC_CHAMELEON_LIBDIR} ${PC_CHAMELEON_LIBRARY_DIRS} )
###

include(FindPackageHandleStandardArgs)
# handle the QUIETLY and REQUIRED arguments and set CHAMELEON_FOUND to TRUE
# if all listed variables are TRUE

find_package_handle_standard_args(CHAMELEON_STATIC DEFAULT_MSG
    CHAMELEON_STATIC_LIBRARY CHAMELEON_STATIC_INCLUDE_DIR)
mark_as_advanced(CHAMELEON_STATIC_LIBRARY CHAMELEON_STATIC_INCLUDE_DIR)

find_package_handle_standard_args(CHAMELEON_SHARED DEFAULT_MSG
    CHAMELEON_SHARED_LIBRARY CHAMELEON_SHARED_INCLUDE_DIR)
mark_as_advanced(CHAMELEON_SHARED_LIBRARY CHAMELEON_SHARED_INCLUDE_DIR)

if (CHAMELEON_STATIC_FOUND AND NOT CHAMELEON_SHARED_FOUND)
    set(CHAMELEON_INCLUDE_DIRS ${PC_CHAMELEON_STATIC_INCLUDE_DIRS} )
    set(CHAMELEON_DEFINITIONS ${PC_CHAMELEON_STATIC_CFLAGS_OTHER} )
    fabulous_find_static_libraries(CHAMELEON PC_CHAMELEON)
elseif(CHAMELEON_SHARED_FOUND)
    set(CHAMELEON_INCLUDE_DIRS ${PC_CHAMELEON_INCLUDE_DIRS} )
    set(CHAMELEON_DEFINITIONS ${PC_CHAMELEON_CFLAGS_OTHER} )
    fabulous_find_shared_libraries(CHAMELEON PC_CHAMELEON)
endif()
find_package_handle_standard_args(
    CHAMELEON
    DEFAULT_MSG
    CHAMELEON_LIBRARIES CHAMELEON_INCLUDE_DIRS)
