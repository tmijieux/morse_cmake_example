Example of use of chameleon with morse_cmake submodule

#+BEGIN_SRC shell
git submodule update --init --recursive
#+END_SRC

#+NAME: workdir
: ~/morse_cmake_example

Compilation:
#+BEGIN_SRC shell :session *install* :results silent :var PROJECT_ROOT=workdir
cd $PROJECT_ROOT
rm -rf build/
mkdir -p build
cd build/
cmake ..
make
#+END_SRC
